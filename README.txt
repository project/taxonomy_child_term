CONTENTS OF THIS FILE
---------------------
 * Overview
 * Installation
 * How to use


Overview
--------
Give link to add taxonomy term child.


Installation
------------
Install as you would normally install a contributed drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.


How to use
----------
Visit overview vocabulary list terms form,
admin/structure/taxonomy/(VOCABULARY) which (VOCABULARY) is 
your machine name of VOCABULARY,
you'll see the add child link.
